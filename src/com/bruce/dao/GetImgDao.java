package com.bruce.dao;

import java.io.Serializable;

import com.bruce.model.GetImg;

public interface GetImgDao extends HibernateDao<GetImg, Serializable> {
	
}