package com.bruce.dao;

import java.io.Serializable;

import com.bruce.model.GetNote;

public interface GetNoteDao extends HibernateDao<GetNote, Serializable> {
	
}