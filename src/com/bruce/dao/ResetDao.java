package com.bruce.dao;

import java.io.Serializable;


import com.bruce.model.Reset;

public interface ResetDao extends HibernateDao<Reset, Serializable> {
	
}